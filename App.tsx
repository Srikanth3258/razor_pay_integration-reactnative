import React, {useState} from 'react';
import {View, Text, TextInput, Alert} from 'react-native';
import RazorpayCheckout from 'react-native-razorpay';

const razorpayKeyId = process.env.RAZORPAY_KEY_ID;
const razorpayKeySecret = process.env.RAZORPAY_KEY_SECRET;

export default function App() {
  const [amount, setAmount] = useState('');

  const handlePayment = () => {
    // @ts-ignore
    if (isNaN(amount) || parseFloat(amount) <= 0) {
      Alert.alert('Please enter a valid amount');
      return;
    }

    console.log('KEY' + razorpayKeyId + 'sec_key' + razorpayKeySecret);

    const options = {
      description: 'Buy BMW CAR',
      image: '',
      currency: 'INR',
      key: razorpayKeyId,

      amount: parseFloat(amount) * 100,
      name: 'test order',
      order_id: '',
      prefill: {
        email: 'xyz@gmail.com',
        contact: '9999999999',
        name: 'User 1',
      },
      theme: {color: '#edba2d'},
    };
    // @ts-ignore
    RazorpayCheckout.open(options)
      .then(data => {
        Alert.alert(`Success: ${data.razorpay_payment_id}`);
      })
      .catch(error => {
        console.log(error);
        Alert.alert(`Error: ${error.code} | ${error.description}`);
      });
  };

  return (
    <View style={{flex: 1, backgroundColor: 'white', padding: 20}}>
      <TextInput
        placeholder="Enter amount"
        keyboardType="numeric"
        value={amount}
        onChangeText={text => setAmount(text)}
      />
      <Text
        onPress={handlePayment}
        style={{
          backgroundColor: 'green',
          color: 'white',
          padding: 10,
          margin: 10,
          textAlign: 'center',
        }}>
        Pay Now
      </Text>
    </View>
  );
}
